After checking out the repository, change the permission of files/private.pem to 600.
Add the IP addresses (reachable ones) to the inventory in 'default' group.

To install packages on those machines, run the following:
$ ansible-play install.yml -e hosts_var=default -e pkg_name=git
$ ansible-play install.yml -e hosts_var=default -e pkg_name=git --limit <IP>
(to install specifically on a single machine)

Recommended software installations before configuring ansible-pull.
$ ansible-play install.yml -e hosts_var=default -e pkg_name=git
$ ansible-play install.yml -e hosts_var=default -e pkg_name=ansible

Configure ansible-pull by running the following:
$ ansible-play play_configure_ansible_pull.yml -e hosts_var=default